import {Inputs} from "./inputs";
import {AreaControls} from "./areaControls";
import {FlatDraw} from "./flatDraw";


export class PlanksDraw {
    static init = (options) => {
        if (!this.instance) {
            this.instance = new this(options);
        }
        return this.instance;
    }

    wallsMinX = null;
    wallsMinY = null;
    wallsMaxX = null;
    wallsMaxY = null;

    constructor(options) {
        this.svg = options.svg;
        this.planks = this.svg.querySelector(":scope > .planks");
        this.svgNS = options.svgNS;
        this.options = options;

        this.inputs = Inputs.init();
        this.controls = AreaControls.init();
        this.flatDraw = FlatDraw.init();

        this.drawPlanks();
        for (let inputName of [
            'lengthRoom', 'widthRoom', 'lengthPl', 'widthPl', 'plankCount',
            'wallOffset', 'firstOffset', 'secondOffset', 'verticalOffset'
        ]) {
            this.inputs.eventEmitters[inputName].subscribe(this.redrawPlanks.bind(this));
        }
    }

    drawPlank(x, y, length, width, n) {
        const circle = document.createElementNS(this.svgNS, 'rect');
        circle.setAttribute('x', x);
        circle.setAttribute('y', y);
        circle.setAttribute('width', length);
        circle.setAttribute('height', width);
        circle.setAttribute('style', 'fill: none; stroke: #888; stroke-width: 3px;');
        // this.controls.addTo(circle);
        this.planks.appendChild(circle);
        const text = document.createElementNS(this.svgNS, 'text');
        text.setAttribute('x', Math.floor(x + length / 2));
        text.setAttribute('y', Math.floor(y + width / 2 + 20));
        text.setAttribute('text-anchor', 'middle');
        const textNode = document.createTextNode('' + n);
        text.appendChild(textNode);
        this.planks.appendChild(text);
    }

    drawPlanks() {
        [
            this.wallsMinX, this.wallsMinY, this.wallsMaxX, this.wallsMaxY
        ] = this.flatDraw.getWallsMinMaxes();
        if (this.wallsMaxX > 100000 || this.wallsMaxY > 100000) {
            return;
        }
        const
            plankL = this.inputs.lengthPl,
            plankW = this.inputs.widthPl,
            offsetWall = this.inputs.wallOffset,
            offsetFirst = this.inputs.firstOffset,
            offsetSecond = this.inputs.secondOffset,
            offsetVertical = this.inputs.verticalOffset;
        let currX = this.wallsMinX, currY = this.wallsMinY,
            firstRow = true, iCol = 0, iRow = 0, n = 0;
        do {
            iRow++;
            if (firstRow) {
                firstRow = false;
                currX += offsetFirst + offsetWall;
                currY += offsetVertical + offsetWall;
            } else {
                // отсчитываем смещение каждого следующего ряда за первым
            }
            do {
                iCol++;
                n++;
                this.drawPlank(currX, currY, plankL, plankW, n);
                currX += plankL;
            } while (currX < this.wallsMaxX - offsetWall);
            currX -= iCol * plankL - offsetSecond;
            if (currX > this.wallsMinX + offsetWall) {
                currX -= plankL;
            }
            iCol = 0;
            currY += plankW;
        } while (currY < this.wallsMaxY - offsetWall);
        // const roomFrom = [
        //     [0, 0],
        //     [this.inputs.lengthRoom, 0],
        //     [this.inputs.lengthRoom, this.inputs.widthRoom],
        //     [0, this.inputs.widthRoom],
        // ];
        // let prevDot = null, isFirst = true;
        // for (let dot of roomFrom) {
        //     if (prevDot) {
        //         this.drawWall(prevDot, dot);
        //         if (isFirst) {
        //             isFirst = false;
        //         } else {
        //             this.drawCircle(prevDot);
        //         }
        //     }
        //     prevDot = dot;
        // }
        // if (prevDot) {
        //     this.drawWall(prevDot, roomFrom[0]);
        //     this.drawCircle(prevDot);
        //     this.drawCircle(roomFrom[0]);
        // }
    }

    redrawPlanks() {
        this.planks.querySelectorAll(":scope > *").forEach(el => el.remove());
        this.drawPlanks();
    }

}
