import {ObsEmitter} from "./obsEmitter";


export class Inputs {
    static init = (options) => {
        if (!this.instance) {
            this.instance = new this(options);
        }
        return this.instance;
    }
    lengthRoom;
    widthRoom;
    lengthPl;
    widthPl;
    plankCount;
    wallOffset;
    firstOffset;
    secondOffset;
    verticalOffset;

    constructor(options) {
        this.options = options;
        this.vals = {};
        this.eventEmitters = {};
        this._inputs = {};
        for (let name of [
            'lengthRoom', 'widthRoom', 'lengthPl', 'widthPl', 'plankCount',
            'wallOffset', 'firstOffset', 'secondOffset', 'verticalOffset',
        ]) {
            Object.defineProperty(this, name, {
                get: () => {
                    return this.vals[name];
                },
                set: (v) => {
                    this.vals[name] = v;
                },
            });
            this._inputs[name] = document.getElementById( name );
            this[name] = Number(this._inputs[name].value);
            const eventName = this._inputs[name].type === 'range' ? 'change' : 'keyup';
            this.eventEmitters[name] = new ObsEmitter();
            this._inputs[name].addEventListener(eventName, e => {
                let num = Number(this._inputs[name].value);
                if (!num) {
                    this._inputs[name].value = this._inputs[name].value.replace(/[^\d]/g, '');
                    num = Number(this._inputs[name].value);
                }
                this[name] = num;
                this.eventEmitters[name].next(num);
            });

        }
    }

}
