
export function ObsEmitter() {
    this.consumers = {};
}

ObsEmitter.prototype.next = function (value) {
    for (let key in this.consumers)
        this.consumers[key](value);
};

ObsEmitter.prototype.subscribe = function (callback) {

    if ("next" in callback)
        callback = callback.next;

    let namespace = Math.random().toString(36).slice(2);
    this.consumers[namespace] = callback;
    return {
        unsubscribe: () => delete this.consumers[namespace]
    };
};
