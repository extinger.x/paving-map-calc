import {Inputs} from "./inputs";
import {AreaControls} from "./areaControls";


export class FlatDraw {
    static init = (options) => {
        if (!this.instance) {
            this.instance = new this(options);
        }
        return this.instance;
    }

    dots = [];

    constructor(options) {
        this.svg = options.svg;
        this.walls = this.svg.querySelector(":scope > .walls");
        this.svgNS = options.svgNS;
        this.options = options;

        this.inputs = Inputs.init();
        this.controls = AreaControls.init();

        this.drawWalls();
        this.inputs.eventEmitters.lengthRoom.subscribe(this.redrawWalls.bind(this));
        this.inputs.eventEmitters.widthRoom.subscribe(this.redrawWalls.bind(this));
    }

    drawWall(d1, d2) {
        const line = document.createElementNS(this.svgNS, 'line');
        line.setAttribute('x1', d1[0]);
        line.setAttribute('y1', d1[1]);
        line.setAttribute('x2', d2[0]);
        line.setAttribute('y2', d2[1]);
        line.setAttribute('style', 'stroke: red; stroke-width: 4px;');
        this.walls.appendChild(line);
    }

    drawCircle(dot) {
        const circle = document.createElementNS(this.svgNS, 'circle');
        circle.setAttribute('cx', dot[0]);
        circle.setAttribute('cy', dot[1]);
        circle.setAttribute('r', 100);
        circle.setAttribute('style', 'fill: #fff; stroke: red; stroke-width: 4px;');
        // this.controls.addTo(circle);
        this.walls.appendChild(circle);
    }

    drawWalls() {
        const roomFrom = [
            [0, 0],
            [this.inputs.lengthRoom, 0],
            [this.inputs.lengthRoom, this.inputs.widthRoom],
            [0, this.inputs.widthRoom],
        ];
        let prevDot = null, isFirst = true;
        this.dots = [];
        for (let dot of roomFrom) {
            this.dots.push(dot);
            if (prevDot) {
                this.drawWall(prevDot, dot);
                if (isFirst) {
                    isFirst = false;
                } else {
                    this.drawCircle(prevDot);
                }
            }
            prevDot = dot;
        }
        if (prevDot) {
            this.drawWall(prevDot, roomFrom[0]);
            this.drawCircle(prevDot);
            this.drawCircle(roomFrom[0]);
        }
    }

    redrawWalls() {
        this.walls.querySelectorAll(":scope > *").forEach(el => el.remove());
        this.drawWalls();
    }

    getWallsMinMaxes() {
        let minX, minY, maxX, maxY;
        for (let dot of this.dots) {
            if (typeof minX === 'undefined' || minX > dot[0]) {
                minX = dot[0];
            }
            if (typeof maxX === 'undefined' || maxX < dot[0]) {
                maxX = dot[0];
            }
            if (typeof minY === 'undefined' || minY > dot[1]) {
                minY = dot[1];
            }
            if (typeof maxY === 'undefined' || maxY < dot[1]) {
                maxY = dot[1];
            }
        }
        return [minX, minY, maxX, maxY];
    }

}
