export class AreaControls {
    static init = (options = null) => {
        if (!this.instance) {
            this.instance = new this(options);
        }
        return this.instance;
    }

    _scale = 1;
    top = -2000;
    left = -2000;

    set scale(v) {
        this._scale = v;
        this.svg.setAttribute('viewBox', this.left + " "
            + this.top + " " + (v * 15000) + " " + (v * 15000));
    }

    constructor(options) {
        this.svg = options.svg;
        this.options = options;
        const resizeListener = () => {
            this.svg.style.width = window.innerWidth + 'px';
            this.svg.style.height = window.innerHeight + 'px';
        }
        resizeListener();
        window.addEventListener('resize', resizeListener);
        window.addEventListener('selectstart', e => e.preventDefault());
        document.addEventListener('wheel', event => {
            if (event.ctrlKey === true) {
                event.preventDefault();
            }
        }, {passive: false});
        options.body.addEventListener('keydown', event => {
            if (event.ctrlKey === true && (event.keyCode === 61 || event.keyCode === 107
                || event.keyCode === 173 || event.keyCode === 109 || event.keyCode === 187
                || event.keyCode === 189 || event.keyCode === 123)) {
                event.preventDefault();
                if (event.keyCode === 123) {
                    this.scale = 1;
                }
            }
        });
        this.svg.addEventListener('contextmenu', e => e.preventDefault());
        this.svg.addEventListener('mousemove', this.mouseMove.bind(this));
        this.svg.addEventListener('mousedown', this.mouseDownSvg.bind(this));
        this.svg.addEventListener('mouseup', this.mouseUp.bind(this));
        this.svg.addEventListener('wheel', this.mouseWheel.bind(this));
        this.scale = 1;
    }

    mouseWheel(event) {
        if (!this.movingViewBox) {
            this.scale = this._scale + event.deltaY * 0.002;
        }
    };

    mouseMove(event) {
        if (this.movingViewBox) {
            this.top -= this.offsetY(event);
            this.left -= this.offsetX(event);
            this.scale = this._scale;
            this.setClientXY(event);
        }
    };

    mouseUp(event) {
        this.movingViewBox = false;
    };

    mouseDownSvg(event) {
        if (event.target === this.svg) {
            this.movingViewBox = true;
            this.setClientXY(event);
        }
    };

    // addTo(circle) {
    //     // circle.addEventListener('mousedown', this.mouseDown);
    // }

    setClientXY(event) {
        this.clientX = event.clientX;
        this.clientY = event.clientY;
    }

    offsetX(event) {
        return (event.clientX - this.clientX) * 20 * this._scale;
    }

    offsetY(event) {
        return (event.clientY - this.clientY) * 20 * this._scale;
    }
}
