import './index.html';
import './index.scss';

import {AreaControls} from "./modules/areaControls";
import {FlatDraw} from "./modules/flatDraw";
import {Inputs} from "./modules/inputs";
import {PlanksDraw} from "./modules/planksDraw";


const svgNS = "http://www.w3.org/2000/svg",
    svg = document.getElementById( 'main-svg' ),
    body = document.getElementById( 'main' );

AreaControls.init({
    svg: svg,
    body: body,
});
Inputs.init({});
FlatDraw.init({
    svg: svg,
    svgNS: svgNS,
});
PlanksDraw.init({
    svg: svg,
    svgNS: svgNS,
});
