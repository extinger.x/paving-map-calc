#!/bin/bash
set -e
npm run build-prod
rsync --delete -azhe ssh ./dist/ api@savelov.site:/home/api/savelov.site/html/paving-map-calc
rm -rf ./dist
